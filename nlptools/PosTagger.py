#!/usr/bin/python
# -*- coding: utf-8 -*-

from felice_maxent import *
import sys
import os.path
import getopt
import re
from felice_libsvm_2_91_svm import *
import codecs
import cPickle
from Conf import Conf
from Token import Token
from Utils import *
from pkg_resources import resource_filename

cliticPosMorph = {
  'ce': ('PC1np', 'ce'),
  'ci': ('PC1np', 'ci'),
  'gli': ('PC3ms', 'gli'),
  'glie': ('PC3ms', 'gli'),
  'la': ('PC3fs', 'la'),
  'le': ('PC3fn', 'le'),
  'li': ('PC3mp', 'li'),
  'lo': ('PC3ms', 'lo'),
  'me': ('PC1ns', 'me'),
  'mi': ('PC1ns', 'mi'),
  'se': ('PC3nn', 'se'),
  'si': ('PC3nn', 'si'),
  'te': ('PC2ns', 'te'),
  'ti': ('PC2ns', 'ti'),
  've': ('PC2np', 've'),
  'vi': ('PC2np', 'vi'),
  'ne': ('PCnn', 'ne'),
  }

fposPattern = re.compile('([A-Z]+)(.*)')


class PosTagger:

  ver = '$RCSfile: PosTagger.py,v $ $Revision: 1.13 $'

  def __init__(self, encode='utf-8'):
    self.debug = False
    self.Formario = {}
    self.Lexicon = {}
    self.dump = False
    self.encode = encode
    self.training = False
    self.lingua = 'IT'

  def setDebug(self, debug):
    self.debug = debug

  def setEncode(self, encode):
    self.encode = encode

  def setFileNameLessico(self, fileNameLessico):
    self.fileNameLessico = fileNameLessico

  def setFileNameFormario(self, fileNameFormario):
    self.fileNameFormario = fileNameFormario

  def setMophSplit(self, morphSplit):
    self.morphSplit = morphSplit

  def setTraining(self, training):
    self.training = training

  def setDump(self, dump):
    self.dump = dump

  def setLingua(self, lingua):
    self.lingua = lingua

  def setPickled(self, pickled):
    self.pickled = pickled

  def caricaFormario(self):
    file_formario = openFile(self.fileNameFormario)
    file_formario_text = file_formario.read()
    for l in iter(file_formario_text.splitlines()):
      tokens = l.strip().split('\t')
      form = tokens[0].lower()
      pos = tokens[1::2]
      lemma = tokens[2::2]
      for index in xrange(len(pos)):
        self.Formario['%s:%s' % (form, pos[index])] = lemma[index].split('|')[0]

  def caricaLessico(self):
    file_lessico = openFile(self.fileNameLessico)
    file_lessico_text = file_lessico.read()
    for l in iter(file_lessico_text.splitlines()):
      ls = l.strip().split('\t')
      ls_0_lower = ls[0].lower()
      self.Lexicon[ls_0_lower] = []
      i = 1
      while i < len(ls):
        self.Lexicon[ls_0_lower].append(ls[i])
        i += 1

  def removeClitics(self, form, clitics):

    for clitic in clitics:
      form = form[:-len(clitic)]
    return form

  def splitPos(self, pos):

    (pos, morph) = fposPattern.match(pos).groups()
    feats = ''
    if len(morph) > 0:
      if pos[0] == 'V':  # verb
        if morph[0] == 'p':  # participle
          if morph[1] == 'p':  # present participle
            feats = 'num=%s|mod=%s' % (morph[2], morph[0])
          else:

                              # past participle

            feats = 'num=%s|mod=%s|gen=%s' % (morph[3], morph[0], morph[2])
        elif morph[0] in ('m', 'd'):

                                     # imperative or conditional

          feats = 'num=%s|per=%s|mod=%s|ten=p' % (morph[2], morph[1], morph[0])
        elif morph[0] in ('f', 'g'):

                                     # infinite or gerundive

          feats = 'mod=%s' % morph[0]
        else:
          feats = 'num=%s|per=%s|mod=%s|ten=%s' % (morph[3], morph[2],
                morph[0], morph[1])
      elif pos in ('PC', 'PE') and morph[0].isdigit():

                                                     # personal pronoun

        feats = 'num=%s|per=%s|gen=%s' % (morph[2], morph[0], morph[1])
      else:

        if len(morph) >= 2:
          feats = 'num=%s|gen=%s' % (morph[1], morph[0])
        else:
          feats = ''

    return (pos, feats)

  def MorfoSplitta(self, tokens):
    tokenNo = 0
    tokens2 = []
    for tok in tokens:

      tokenNo += 1
      pos = tok.pos
      form = tok.word
      lemma = tok.lemma

      unknown = False
      if lemma == '<unknown>' or '-' in form and form == lemma:
        unknown = True
        lemma = form

      if pos[-1] != 'c' or unknown:

        (fpos, morph) = self.splitPos(pos)
        if not morph:
          morph = '_'
        tok = Token([
          tokenNo,
          form,
          lemma,
          fpos[0],
          fpos,
          morph,
          ])

        tokens2.append(tok)
        continue

      toks = lemma.split('|')[0].split('-')
      if len(toks) == 1:

        tok = Token([
          tokenNo,
          form,
          lemma,
          'S',
          'SP',
          '_',
          ])

        tokens2.append(tok)
        continue

      verb = self.removeClitics(form, toks[1:])
      (fpos, morph) = self.splitPos(pos[:-1])
      if not morph:
        morph = '_'  # CoNLL empty
      tok = Token([
        tokenNo,
        verb + '-',
        toks[0],
        fpos[0],
        fpos,
        morph,
        ])

      tokens2.append(tok)

      for clitic in toks[1:-1]:
        (pos, lemma) = cliticPosMorph[clitic]
        (fpos, morph) = self.splitPos(pos)
        if not morph:
          morph = '_'  # CoNLL empty
        tokenNo += 1
        tok = Token([
          tokenNo,
          clitic + '-',
          lemma,
          fpos[0],
          fpos,
          morph,
          ])
        tokens2.append(tok)

      (pos, lemma) = cliticPosMorph[toks[-1]]
      (fpos, morph) = self.splitPos(pos)
      if not morph:
        morph = '_'  # CoNLL empty
      tokenNo += 1
      tok = Token([
        tokenNo,
        toks[-1],
        lemma,
        fpos[0],
        fpos,
        morph,
        ])

      tokens2.append(tok)
    return tokens2

  def en_MorfoSplitta(self, tokens):
    tokenNo = 0
    tokens2 = []
    for tok in tokens:
      tokenNo += 1
      pos = tok.pos
      form = tok.word
      if self.Formario:
        lemma = tok.lemma

        unknown = False

        if lemma == '<unknown>' or '-' in form and form == lemma:
          lemma = form
      else:
        lemma = '_'
      tok = Token([
        tokenNo,
        form,
        lemma,
        pos[0:2],
        pos,
        '_',
        ])
      tokens2.append(tok)
    return tokens2

  def estraiFeatures(self, id, tokens):
    ListaFeatures = []

    if self.conf.featValue['LESSICO']:
      for feat in self.conf.featValue['LESSICO']:
        if id + feat >= 0 and id + feat < len(tokens):
          if tokens[id + feat].word.lower() in self.Lexicon:
            nomiPropri = False
            for tag in self.Lexicon[tokens[id + feat].word.lower()]:
              feature = 'LE%d_' % feat + '%s' % tag
              ListaFeatures.append(feature)
              if tag in self.conf.featValue['NOMI_PROPRI']:
                nomiPropri = True
            if not nomiPropri and tokens[id + feat].word[0].isupper() and not id\
               + feat == 0:  # prova2

              for tag in self.conf.featValue['NOMI_PROPRI']:
                feature = 'LE%d_' % feat + '%s' % tag
                ListaFeatures.append(feature)
          else:
            feature = 'LE%d_' % feat + '%s' % 'UNK'
            ListaFeatures.append(feature)

    if self.conf.featValue['LEMMA']:
      for feat in self.conf.featValue['LEMMA']:
        if id + feat >= 0 and id + feat < len(tokens):
          feature = 'Le%d_' % feat + '%s' % tokens[id + feat].lemma
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD']:
      for feat in self.conf.featValue['WORD']:
        if id + feat >= 0 and id + feat < len(tokens):
          feature = 'W%d_' % feat + '%s' % tokens[id + feat].word.lower()
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD_LENGTH']:
      for feat in self.conf.featValue['WORD_LENGTH']:
        if id + feat >= 0 and id + feat < len(tokens):
          feature = 'L%d_' % feat + '%d' % len(tokens[id + feat].word)
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD_SUFFIX']:
      for feat in self.conf.featValue['WORD_SUFFIX']:
        if id + feat >= 0 and id + feat < len(tokens):
          i = 1
          while i < len(tokens[id + feat].word) and i < self.conf.maxSuffix + 1:
            feature = 's%d_' % feat + '%s' % tokens[id
                   + feat].word[len(tokens[id + feat].word) - i:].lower()
            ListaFeatures.append(feature)
            i += 1

    if self.conf.featValue['WORD_PREFIX']:
      for feat in self.conf.featValue['WORD_PREFIX']:
        if id + feat >= 0 and id + feat < len(tokens):
          i = 1
          while i < len(tokens[id + feat].word) and i < self.conf.maxPrefix + 1:
            feature = 'p%d_' % feat + '%s' % tokens[id
                   + feat].word[:-(len(tokens[id + feat].word) - i)].lower()
            ListaFeatures.append(feature)
            i += 1

    if self.conf.featValue['PUNCT_DISTANCE']:
      for feat in self.conf.featValue['PUNCT_DISTANCE']:
        if id + feat >= 0 and id + feat < len(tokens):
          feature = 'D%d_' % feat + '%d' % tokens[id + feat].id
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD_FORMAT']:
      for feat in self.conf.featValue['WORD_FORMAT']:
        if id + feat >= 0 and id + feat < len(tokens):
          format = 0
          if tokens[id + feat].word.islower():
            format = 1
          elif tokens[id + feat].word.isupper():
            format = 3
          elif tokens[id + feat].word[0].isupper() and tokens[id
                 + feat].word[1:].islower():
            format = 2
          feature = 'F%d_' % feat + '%d' % format
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD_SHAPE']:
      for feat in self.conf.featValue['WORD_SHAPE']:
        if id + feat >= 0 and id + feat < len(tokens):
          shape = ''
          i = 0
          while i < len(tokens[id + feat].word):
            if tokens[id + feat].word[i].islower():
              shape += 'x'
            elif tokens[id + feat].word[i].isupper():
              shape += 'X'
            else:
              shape += tokens[id + feat].word[i]
            i += 1
          feature = 'S%d_' % feat + '%s' % shape
          ListaFeatures.append(feature)

    if self.conf.featValue['POS']:
      for feat in self.conf.featValue['POS']:
        if id + feat >= 0 and id + feat < len(tokens):
          feature = 'P%d_' % feat + '%s' % tokens[id + feat].pos
          ListaFeatures.append(feature)

    if self.conf.featValue['WORD_BIGRAM']:
      for feat in self.conf.featValue['WORD_BIGRAM']:
        feat_big = feat.split('_')
        primo_id = int(feat_big[0])
        secondo_id = int(feat_big[1])
        if id + primo_id >= 0 and id + primo_id < len(tokens) and id\
           + secondo_id >= 0 and id + secondo_id < len(tokens):
          feature = 'BW%s_' % feat + '%s' % tokens[id + primo_id].word + '_%s'\
             % tokens[id + secondo_id].word
          ListaFeatures.append(feature)
    if self.conf.featValue['WORD_TRIGRAM']:
      for feat in self.conf.featValue['WORD_TRIGRAM']:
        feat_trig = feat.split('_')
        primo_id = int(feat_trig[0])
        secondo_id = int(feat_trig[1])
        terzo_id = int(feat_trig[2])
        if id + primo_id >= 0 and id + primo_id < len(tokens) and id\
           + secondo_id >= 0 and id + terzo_id < len(tokens) and id + terzo_id\
           >= 0 and id + terzo_id < len(tokens):
          feature = 'TW%s_' % feat + '%s' % tokens[id + primo_id].word + '_%s'\
             % tokens[id + secondo_id].word + '_%s' % tokens[id + terzo_id].word
          ListaFeatures.append(feature)
    if self.conf.featValue['POS_BIGRAM']:
      for feat in self.conf.featValue['POS_BIGRAM']:
        feat_big = feat.split('_')
        primo_id = int(feat_big[0])
        secondo_id = int(feat_big[1])
        if id + primo_id >= 0 and id + primo_id < len(tokens) and id\
           + secondo_id >= 0 and id + secondo_id < len(tokens):
          feature = 'BP%s_' % feat + '%s' % tokens[id + primo_id].pos + '_%s'\
             % tokens[id + secondo_id].pos
          ListaFeatures.append(feature)
    if self.conf.featValue['POS_TRIGRAM']:
      for feat in self.conf.featValue['POS_TRIGRAM']:
        feat_trig = feat.split('_')
        primo_id = int(feat_trig[0])
        secondo_id = int(feat_trig[1])
        terzo_id = int(feat_trig[2])
        if id + primo_id >= 0 and id + primo_id < len(tokens) and id\
           + secondo_id >= 0 and id + secondo_id < len(tokens) and id + terzo_id\
           < len(tokens) and id + terzo_id >= 0 and id + terzo_id < len(tokens):
          feature = 'TP%s_' % feat + '%s' % tokens[id + primo_id].pos + '_%s'\
             % tokens[id + secondo_id].pos + '_%s' % tokens[id + terzo_id].pos
          ListaFeatures.append(feature)

    if self.conf.featValue['SUPER_FEAT']:
      for feat in self.conf.featValue['SUPER_FEAT']:
        feat_S = feat.split('_')
        completo = 1
        Features = []
        feature = 'SB'
        Features.append(feature)
        for f in feat_S:
          feat_id = int(f[1:])
          Type = f[0]
          if id + feat_id >= 0 and id + feat_id < len(tokens):
            if Type == 'W':
              for i in range(len(Features)):
                Features[i] = Features[i] + '_%s_' % f + '%s' % tokens[id
                       + feat_id].word

            if Type == 'l':
              for i in range(len(Features)):
                Features[i] = Features[i] + '_%s_' % f + '%d' % len(tokens[id
                       + feat_id].word)

            if Type == 'P':
              for i in range(len(Features)):
                Features[i] = Features[i] + '_%s_' % f + '%s' % tokens[id
                       + feat_id].pos
            if Type == 'D':
              for i in range(len(Features)):
                Features[i] = Features[i] + '_%s_' % f + '%d' % tokens[id
                       + feat_id].id

            if Type == 'L':
              for i in range(len(Features)):
                fff = Features[i]
                primo = True
                if tokens[id + feat_id].word.lower() in self.Lexicon:
                  for tag in self.Lexicon[tokens[id + feat_id].word.lower()]:

                    if primo:
                      Features[i] = fff + '_%s_' % f + '%s' % tag
                      primo = False
                    else:
                      Features.append(fff + '_%s_' % f + '%s' % tag)
                else:
                  Features[i] += '_%s_' % f + '%s' % 'UNK'
            if Type == 'e':  # and id+feat_id < feat_id):
              for i in range(len(Features)):
                Features[i] = Features[i] + '_%s_' % f + '%s' % tokens[id
                       + feat_id].lemma
          else:
            completo = 0
            break
        if completo:
          for feature in Features:
            ListaFeatures.append(feature)
          Features = []

    return ListaFeatures

  def Training(self, inputFileName):
    labels = []
    samples = []
    contatore = 0.0
    tokens = []
    self.inputFileName = inputFileName
    self.fileInput = codecs.open(inputFileName, encoding=self.encode, mode='r')
    id = 0
    for l in self.fileInput:
      l = l  # .decode('utf-8')  #Enc
      if l == '':
        break
      if l == '\n':  #   or ( l == '' and len(tokens)>0):
        id = 0
        for tok in tokens:

          labels.append(tok.pos)

          ListaFeatures = self.estraiFeatures(tok.id - 1, tokens)
          samples.append(ListaFeatures)
        tokens = []
        continue

      id += 1
      tok = Token(l.split(), id)
      tokens.append(tok)
      contatore += 1.0
      if not contatore % 1000:
        print >> sys.stderr, '+',
      elif not contatore % 100:
        print >> sys.stderr, '.',
    print >> sys.stderr
    print >> sys.stderr, 'Inizio fase di Creazione del Modello'
    LabelsSamples = (labels, samples)
    if self.conf.ALG == 'SVM':
      modello = Fsvm_CreaModelloSVM(LabelsSamples, self.conf.parametriSVM,
                                    self.modello)  # in felice_svm
    elif self.conf.ALG == 'ME':
      modello = Fme_CreaModelloME(LabelsSamples, self.conf.parametriME,
                                  self.modello)  # in felice_maxent
    else:
      print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:', \
        self.conf.ALG
      usage()
      exit(-1)

  def Parsing(self, inputFileName, destDir=None):
    self.inputFileName = inputFileName
    (self.fileInput, self.fileOutput, self.outputFileName) = \
      openIOFiles(inputFileName, destDir, '.pos.tmp')

    tokens = []
    ListaFeatures = []
    id = 0
    while True:
      l = self.fileInput.readline()
      if (l.startswith('<doc') or l.startswith('</doc>')) and len(tokens) == 0:
        print >> self.fileOutput, l.strip()
        print >> self.fileOutput
        tokens = []
        continue
      if l == '\n' and tokens == []:
        continue
      if l == '\n' or l == '' and len(tokens) > 0:  # sono in fondo alla frase => elaboro
        id = 0
        ListaFeatures = []
        for tok in tokens:

          if self.debug:
            print >> sys.stderr, 'test: ', '\t'.join([tok.word.encode('utf-8'),
                  tok.pos.encode('utf-8')])  # test

          if not tok.word[0].isupper() and self.conf.prediligiLessico\
             and (tok.word in self.Lexicon or tok.word.lower() in self.Lexicon)\
             and len(self.Lexicon[tok.word.lower()]) == 1:

            if self.dump:
              print 'Da Lessico: Unica possibile uscita morfologica'
              print
            if tok.word in self.Lexicon:
              tokens[tok.id - 1].pos = self.Lexicon[tok.word][0]
            else:
              tokens[tok.id - 1].pos = self.Lexicon[tok.word.lower()][0]
          else:
            ListaFeatures = self.estraiFeatures(tok.id - 1, tokens)

            if self.debug:
              print 'ListaFeatures', ListaFeatures

            if self.dump:
              for feat in ListaFeatures:
                print feat.encode('utf-8')
              print
            if self.conf.ALG == 'SVM':
              ProbabilityPrediction = Fsvm_ApplicaSVM(ListaFeatures,
                    self.modello, self.modelloIndici)

              if self.conf.MostraProbabilita:
                tokens[tok.id - 1].pos = ProbabilityPrediction[1]
              else:
                tokens[tok.id - 1].pos = ProbabilityPrediction[0]
            elif self.conf.ALG == 'ME':
              ProbabilityPrediction = Fme_ApplicaME(ListaFeatures,
                    self.modello, self.conf.MostraProbabilita)
              if self.conf.MostraProbabilita:
                tokens[tok.id - 1].pos = ProbabilityPrediction[0][0]
              else:
                tokens[tok.id - 1].pos = ProbabilityPrediction
            else:
              print >> sys.stderr, \
                'ERR: Algoritmo di apprendimento sconosciuto:', self.conf.ALG
              usage()

          if tok.word[0].isupper() and not tok.id == 1:  # and not(tok.word.isupper()):
            controlla = True
            if tok.word.isupper():
              if len(tokens) > tok.id:
                if tokens[tok.id - 2].word.isupper()\
                   and tokens[tok.id].word.isupper():
                  controlla = False
              else:
                if tokens[tok.id - 2].word.isupper():
                  controlla = False
            if controlla:
              for tag in self.conf.featValue['FORZA_NOMI_PROPRI']:
                if tok.pos[0] == tag[0]:  # and tok.word[0].isupper() and not(tok.id==1) and not(tok.word.isupper()):
                  tok.pos = self.conf.featValue['NOMI_PROPRI'][0]
                  continue

          if self.Formario:

            chiave = tok.word.lower() + ':' + tok.pos

            if tok.pos in self.conf.featValue['NOMI_PROPRI']:
              tok.lemma = tok.word
            elif chiave in self.Formario:
              tok.lemma = self.Formario[chiave]
            else:
              tok.lemma = '<unknown>'

        if self.morphSplit:
          if self.lingua == 'IT' and self.Formario:
            tokens = self.MorfoSplitta(tokens)
          elif self.lingua == 'EN':
            tokens = self.en_MorfoSplitta(tokens)

        self.StampaOut(tokens, self.fileOutput)
        tokens = []
        continue
      if l == '':
        break
      id += 1
      tok = Token(l.split(), id)
      tokens.append(tok)

    self.outputFileName = renameTmpFileName(self.outputFileName)

  def StampaOut(self, tokens, fileOut=sys.stdout):
    for tok in tokens:
      if self.morphSplit:
        print >> fileOut, '\t'.join([  # Enc
          str(tok.id),
          tok.word,
          tok.lemma,
          tok.cpos,
          tok.pos,
          tok.mfeats,
          ])
      elif self.Formario:
        print >> fileOut, '\t'.join([tok.word, tok.pos, tok.lemma])  # Enc
      else:
        print >> fileOut, '\t'.join([tok.word, tok.pos])  # Enc
    print >> fileOut

  def CaricaPosTagger(self, confFileName, modelFileName):
    self.conf = Conf()
    self.conf.CaricaFileConfigurazione(confFileName)

    try:
      if self.fileNameLessico:
        self.caricaLessico()

      if self.training:
        self.modello = modelFileName
      else:
        if self.fileNameFormario:
          self.caricaFormario()
        if self.conf.ALG == 'SVM':
          self.modello = Fsvm_CaricaModelloSVM(modelFileName)  # in felice_svm
          self.modelloIndici = Fsvm_CaricaIndici(modelFileName)
        elif self.conf.ALG == 'ME':
          self.modello = MaxentModel()
          self.modello.load(modelFileName + '.me')
        else:
          usage()
          print >> sys.stderr, 'ERR: Algoritmo di apprendimento sconosciuto:', \
            self.conf.ALG
          exit(-1)
    except KeyError:
      print >> sys.stderr, 'Unknown key', sys.exc_info()[1], self.conf.featValue
      exit(-1)

  def picklingLexicons(self):

    print >> sys.stderr, 'fileNameLessico:', self.fileNameLessico
    with open(self.fileNameLessico + '.pickled', 'wb') as f:
      cPickle.dump(self.Lexicon, f, 2)
    print >> sys.stderr, 'fileNameFormario:', self.fileNameFormario
    with open(self.fileNameFormario + '.pickled', 'wb') as f:
      cPickle.dump(self.Formario, f, 2)

  def PosTagging(self, inputFileName, destDir=None):
    self.Parsing(inputFileName, destDir)
    return self.outputFileName


def usage():
  print """PosTagger:
	Il file di training deve essere un file verticalizzato con due colonne per riga:
	   form   pos
	Il file di test deve essere un file verticalizzato con una forma per ogni riga:
	   form
	Sia nel training set che nel test set, frasi diverse vanno separate con un rigo bianco.

	Usage:
	  PosTagge.py [options] < [file]

	Options:
	  -t                    training mode
	  -i inputFile          input file name
	  -m file.model         modelFile
	  -d                    print the value of the features
	  -c confile.conf       set configuration file
	  -l LexiconFile        set the lexicon file (used in analysis step)
	  -f formario           set the lexicon file (used in lemmatization step)
	  -M                    split clitics and set output in conll formt
	  -L                    set language (IT/EN), default IT
	  --help                display this help and exit
	  --usage               display script usage
          -p|--pickling         save lexicons in pickle binary format

	"""


def main():
  MODULE_NAME = "t2k.catenapy.PosTagger"
  try:
    long_opts = ['help', 'usage', 'pickling']
    (opts, args) = getopt.gnu_getopt(sys.argv[1:], 'hL:c:dtm:l:f:i:Mp',
                                     long_opts)
  except getopt.GetoptError:
    usage()
    sys.exit()
  scriptpath = os.path.abspath(os.path.dirname(sys.argv[0]))

  debug = False
  encode = 'utf-8'
  dump = False
  fileNameLessico = resource_filename(MODULE_NAME, 'Lessici/it/PosTagger/lexicon.tanl')
  fileNameFormario = resource_filename(MODULE_NAME, 'Lessici/it/PosTagger/fullexMorph.tanl')
  fileConf =  resource_filename(MODULE_NAME, 'Conf/it/postaggerME.conf')
  training = False
  printText = False
  morphSplit = False
  inputFileName = ''
  lingua = 'IT'
  pickling = False
  modelFile = \
       resource_filename(MODULE_NAME, 'Modelli/it/PosTagger/modelloPOS_conLessico_isst-paisa-devLeg-testLeg')
  for (opt, arg) in opts:
    if opt in ('-h', '--help'):
      usage()
      sys.exit()
    elif opt == '--usage':
      usage()
      sys.exit()
    elif opt == '-t':
      training = True
    elif opt == '-c':
      fileConf = arg
    elif opt == '-m':
      modelFile = arg
    elif opt == '-l':
      fileNameLessico = arg
    elif opt == '-f':
      fileNameFormario = arg
    elif opt == '-i':
      inputFileName = arg
    elif opt == '-M':
      morphSplit = True
    elif opt == '-d':
      dump = True
    elif opt == '-L':
      lingua = arg
    elif opt in ('-p', '--pickling'):
      pickling = True
    else:
      assert False, 'unhandled option'

  p = PosTagger()
  p.setDebug(debug)
  p.setEncode(encode)

  p.setFileNameLessico(fileNameLessico)

  p.setFileNameFormario(fileNameFormario)
  p.setMophSplit(morphSplit)
  p.setTraining(training)
  p.setDump(dump)
  p.setLingua(lingua)

  p.CaricaPosTagger(fileConf, modelFile)

  if pickling:
    p.picklingLexicons()
  else:
    if training:
      p.Training(inputFileName)
    else:
      p.PosTagging(inputFileName)


if __name__ == '__main__':
  main()

