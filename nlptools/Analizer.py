import os
import sys
import re
import getopt
import codecs
import traceback
import string
import ConfigParser
import logging
import SentenceSplitter
from pkg_resources import resource_filename
from riallineadoc import riallinea

MODULE_NAME = "Analizer"

import os

try:
    from SentenceSplitter import SentenceSplitter
except ImportError:
    print >> sys.stderr,"Warn: no SentenceSplitter"
try:
    from Tokenizer import Tokenizer
except ImportError:
    print >> sys.stderr,"Warn: no Tokenizer"
try:
    from PosTagger import PosTagger
except ImportError:
    print >> sys.stderr,"Warn: no PosTagger"
try:
    from Parser import ParserPY
except ImportError:
    print >> sys.stderr,"Warn: no ParserPY"
try:
    from Monitoraggio import Monitoraggio
except ImportError:
    print >> sys.stderr,"Warn: no Monitoraggio"

try:
    from Proiezione import Proiezione
except ImportError:
    print >> sys.stderr,"Warn: no Proiezione"
from Utils import *


class ConfReadability:

    def __init__(self):
        pass

class ConfAnalizer:

    config = None
    encode = ''
    debug = False
    useLexicon = True
    tokenizerRule = ''
    lessico = ''
    levels = getLevels("pa")
    pickled = False

    ver = "$Id: Analizer.py,v 1.19 2012-04-17 11:08:05 simone Exp $"

    def __init__(self):
        self.config = ConfigParser.RawConfigParser()
        self.configRe = {}

    def parseReadabilityConfig(self,config, type):
   
        section = 'Readability_' + type

        conf = ConfReadability()

        try:
            conf.R_lessico = resource_filename(MODULE_NAME, config.get(section, 'lessico'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_modello = resource_filename(MODULE_NAME, config.get(section, 'modello'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_configurazione = resource_filename(MODULE_NAME, config.get(section, 'configurazione'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_printText = str2bool(config.get(section, 'printText'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_training = str2bool(config.get(section, 'training'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_dump = str2bool(config.get(section, 'dump'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_infLength = int(config.get(section, 'infLength'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"
        try:
            conf.R_infSenteces = int(config.get(section, 'infSenteces'))
        except ConfigParser.NoOptionError, w:
            print >> sys.stderr, "Warning:", w, "... Use default value"

            conf.type = type
        return conf

    def readConfig(self, filename, levels=None, pickled="False"):
        try:
            self.config.read(filename)
            try:
                self.encode = self.config.get('Global', 'encode')
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                self.debug =  str2bool(self.config.get('Global', 'debug'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                if levels:
                    self.levels =  getLevels(levels)
                else:
                    self.levels =  getLevels(self.config.get('Global', 'levels'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                if pickled:
                    self.pickled =  str2bool(pickled)
                else:
                    self.pickled =  str2bool(self.config.get('Global', 'pickled'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

#SentenceSplitter
            try:
                self.S_ritorniACapo =  str2bool(self.config.get('SentenceSplitter', 'ritorniACapo'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.S_configurazione = resource_filename(MODULE_NAME, self.config.get('SentenceSplitter', 'configurazione'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.S_modello =  resource_filename(MODULE_NAME, self.config.get('SentenceSplitter', 'modello'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

#Normalizer
            try:
                self.N_useNormalizer = str2bool(self.config.get('Normalizer', 'useNormalizer'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                self.N_normalizerRuleFileName = resource_filename(MODULE_NAME, self.config.get('Normalizer', 'normalizerRuleFileName'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

#Tokenizer
            try:
                self.T_useLexicon = str2bool(self.config.get('Tokenizer', 'useLexicon'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                self.T_tokenizerRule = resource_filename(MODULE_NAME, self.config.get('Tokenizer', 'rule'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

            try:
                self.T_fileParoleAccentate = resource_filename(MODULE_NAME, self.config.get('Tokenizer', 'fileParoleAccentate'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
                self.T_fileParoleAccentate = None
#PosTagger
            try:
                self.PT_lessico = resource_filename(MODULE_NAME, self.config.get('PosTagger', 'lessico'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_formario = resource_filename(MODULE_NAME, self.config.get('PosTagger', 'formario'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_configurazione = resource_filename(MODULE_NAME, self.config.get('PosTagger', 'configurazione'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_modello = resource_filename(MODULE_NAME, self.config.get('PosTagger', 'modello'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_morphSplit = str2bool(self.config.get('PosTagger', 'morphSplit'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_training = str2bool(self.config.get('PosTagger', 'training'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_dump = str2bool(self.config.get('PosTagger', 'dump'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.PT_lingua = self.config.get('PosTagger', 'lingua')
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"


#Parser
            try:
                self.Pa_modello = resource_filename(MODULE_NAME, self.config.get('Parser', 'modello'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

#Readability
            #print >> sys.stderr,"levels:", self.levels
            for type in ('rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg'):
                if (self.levels[type] == 1):
                    #print >> sys.stderr,"readability level:", type
                    self.configRe[type] = self.parseReadabilityConfig(self.config,type)

#Monitoraggio
            try:
                self.Mo_lessico = resource_filename(MODULE_NAME, self.config.get('Monitoraggio', 'lessico'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"
            try:
                self.Mo_confronto = str2bool(self.config.get('Monitoraggio', 'confronto'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

#Proiezione
            try:
                self.Pr_dizionario = resource_filename(MODULE_NAME, self.config.get('Proiezione', 'lessico'))
            except ConfigParser.NoOptionError, w:
                print >> sys.stderr, "Warning:", w, "... Use default value"

        except ConfigParser.Error, e:
          import pdb; pdb.set_trace()
          print >> sys.stderr, "Error parsing analizer config file:", e, filename
          sys.exit(-1)

    def printConf(self):
        for property, value in vars(self).iteritems():
            print property, ":", value


    @staticmethod
    def show_ver():
        print >> sys.stderr,self.ver


class Analizer:

#    config = None
#    sentenceSplitter = None
#    tokenizer = None
#    posTagger = None

    ver = "$RCSfile: Analizer.py,v $ $Revision: 1.19 $"
    def __init__(self):
        self.logger = None

    def loadConfig(self, filename, levels = None, pickled = "False"):
        self.config = ConfAnalizer()
        self.config.readConfig(filename, levels, pickled)


    def newReadability (self, config):
        readability = Readability()
        readability.setDebug(True)
        readability.setLessico(config.R_lessico)
        readability.setTraining(config.R_training)
        readability.setPrintText(config.R_printText)
        readability.setDump(config.R_dump)
        readability.setInfLength(config.R_infLength)
        readability.setInfSenteces(config.R_infSenteces)
        #readability.setPickled(config.pickled)

        return readability

    def load(self):

        if (not self.config.levels):
            print >> "Unknown level:", sys.stderr,self.config.levels
            if self.logger:
                self.logger.error("Unknown level")

        #Sentence Splitter
        if (self.config.levels["ss"] == 1):
            self.sentenceSplitter = SentenceSplitter()
            self.sentenceSplitter.setDebug(self.config.debug)
            self.sentenceSplitter.setEncode(self.config.encode)
            self.sentenceSplitter.setPickled(self.config.pickled)
            self.sentenceSplitter.setRitorniACapo(self.config.S_ritorniACapo)

            self.sentenceSplitter.CaricaSentenceSplitter(self.config.S_configurazione, self.config.S_modello)
            if self.logger:
                self.logger.info("Sentence Splitter loaded")
            if(self.config.debug):
                print >> sys.stderr, "Sentence Splitter loaded"

        #Tokenizer
        if (self.config.levels["tk"] == 1):
            self.tokenizer = Tokenizer()
            self.tokenizer.setDebug(self.config.debug)
            self.tokenizer.setEncode(self.config.encode)
            self.tokenizer.setUseLexicon(self.config.T_useLexicon)
        #self.tokenizer.setOutputFile("")
        #self.tokenizer.printConf()
            self.tokenizer.setUseNormalizer(self.config.N_useNormalizer)
            self.tokenizer.setNormalizerRuleFileName(self.config.N_normalizerRuleFileName)
            self.tokenizer.CaricaTokenizer(self.config.T_tokenizerRule,
                                           self.config.T_fileParoleAccentate)
            if(self.config.debug):
                print >> sys.stderr, "Tokenizer loaded"

        #PosTagger
        if (self.config.levels["pt"] == 1):
            self.posTagger = PosTagger()
            self.posTagger.setDebug(self.config.debug)
            self.posTagger.setEncode(self.config.encode)
            self.posTagger.setFileNameLessico(self.config.PT_lessico)
            self.posTagger.setFileNameFormario(self.config.PT_formario)
            self.posTagger.setMophSplit(self.config.PT_morphSplit)
            self.posTagger.setTraining(self.config.PT_training)
            self.posTagger.setDump(self.config.PT_dump)
            self.posTagger.setLingua(self.config.PT_lingua)
            self.posTagger.setPickled(self.config.pickled)

            self.posTagger.CaricaPosTagger(self.config.PT_configurazione,self.config.PT_modello)
            if(self.config.debug):
                print >> sys.stderr, "Pos Tagger loaded"

        #Parser
        if (self.config.levels["pa"] == 1 ):
            self.parser = ParserPY()
            self.parser.setDebug(self.config.debug)
            self.parser.CaricaParser(self.config.Pa_modello)
            if(self.config.debug):
                print >> sys.stderr, "Parser loaded"


        #Readability
        #print >> sys.stderr, "levels:", self.config.levels
        self.readabilities = dict()
        for type in ('rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg'):
            if (self.config.levels[type] == 1):
#                print >> sys.stderr, "Readability %s" % type
#                print >> sys.stderr, "Readability", self.config.configRe[type].R_configurazione,self.config.configRe[type].R_modello
                self.readabilities[type] = self.newReadability(self.config.configRe[type])
                self.readabilities[type].CaricaReadability(self.config.configRe[type].R_configurazione,self.config.configRe[type].R_modello, type)
                if(self.config.debug):
                    print >> sys.stderr, "Readability %s loaded" % type

        #Monitoraggio
        if (self.config.levels["mo"] == 1):
            self.monitoraggio = Monitoraggio()
            self.monitoraggio.setLessico(self.config.Mo_lessico)
            self.monitoraggio.setConfronto(self.config.Mo_confronto)
            self.monitoraggio.CaricaMonitoraggio()

        #Proiezione
        if (self.config.levels["pr"] == 1):
            self.proiezione = Proiezione()
            self.proiezione.setLessico(self.config.Pr_dizionario)
            self.proiezione.CaricaProiezione()

    def run(self, inputFileName, destDir, levels = None):
        #print >> sys.stderr,"run started"
        if(self.config.debug):
            print >> sys.stderr, "Run start"
        if not levels:
            levels = self.config.levels

        if (levels["ss"] == 1):#nuovo nome da passare ai tool temp_+inputFileName
            nextFileName = self.sentenceSplitter.SentenceSplitting(inputFileName, destDir)
        if (levels["tk"] == 1):
            nextFileName = self.tokenizer.Tokenize(nextFileName, destDir)
        if (levels["pt"] == 1):
            nextFileName = self.posTagger.PosTagging(nextFileName, destDir)
        if (levels["pa"] == 1):
            parsedFileName = self.parser.ParsingWithProcess(nextFileName, destDir)
            riallinea(nextFileName, parsedFileName, destDir)
        for type in  ('rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg'):
            if (levels[type] == 1):
#                print >> sys.stderr, "Readability %s is running" % type
#                print >> sys.stderr, "Readability %s " % self.readabilities
                self.readabilities[type].Readability(parsedFileName, destDir)
        if (levels["mo"] == 1):
            self.monitoraggio.Monitoraggio(parsedFileName, destDir)
        if (levels["pr"] == 1):
            self.proiezione.Proiezione(parsedFileName, destDir)

        if(self.config.debug):
            print >> sys.stderr, "Run end"

    def setLogger(self,l):
        self.logger = l

def show_ver():
    print >> sys.stderr, "Version:", Analizer.ver
    print >> sys.stderr, "Version:", SentenceSplitter.ver
    print >> sys.stderr, "Version:", Tokenizer.ver
    print >> sys.stderr, "Version:", PosTagger.ver
    print >> sys.stderr, "Version:", ParserPY.ver
    print >> sys.stderr, "Version:", Readability.ver
    print >> sys.stderr, "Version:", Monitoraggio.ver
    print >> sys.stderr, "Version:", Proiezione.ver

def show_help(outfile, scriptname):
    print >> outfile, 'Usage: python %s [-c analizer.cfg]\n\t\t\t  [-i|--input inputFileName]\n\t\t\t  [--ver]\n\t\t\t  [-l|--level strLevel1[,strlevel2...]]\n\t\t\t  [-s|--srcdir source dir]\n\t\t\t  [-d|--dstdir destination dir]' % scriptname


def main():

    scriptname = os.path.basename(sys.argv[0])
    confFileName = resource_filename(MODULE_NAME, 'analizer.cfg')
    inputFileName = None
    levels = None
    sourceDir = None
    destDir = None

    try:
        long_opts = ['help', 'ver', 'input=', 'levels=', "srcdir=", "dstdir="]
        opts, args = getopt.gnu_getopt(sys.argv[1:], 'i:hc:l:s:d:v',long_opts)
    except getopt.GetoptError, err:
        print >> sys.stderr, err
        show_help(sys.stderr, scriptname)
        sys.exit(1)

    for opt, arg in opts:
        if opt in ('-h', '--help'):
            show_help(sys.stderr, scriptname)
            exit(2)
        elif opt == '-v' or opt == '--ver':
            show_ver()
            exit(2)
        elif opt == '-l' or opt == '--levels' :
            levels = arg
            print >> sys.stderr, "Warning: Level from configuration file will be override:",levels, "levels"
        elif opt == '-c':
            confFileName = arg
        elif opt == '-s' or opt == '--srcdir':
            sourceDir = arg
        elif opt == '-d' or opt == '--dstdir':
            destDir = arg
        elif opt in ('-i', '--input'):
            inputFileName = arg

        else:
            print >> sys.setderr, "argomento errato: ",opt


    if (inputFileName and sourceDir) or (not inputFileName and not sourceDir):
        print >> sys.stderr, "Specifica o il nome del file o della directory"
        sys.exit(1)

    ### Caricamento di Analyzer ###
    analizer = Analizer()
    analizer.loadConfig(confFileName, levels)
    #analizer.config.printConf()
    analizer.load()


    if inputFileName:
        if not destDir:
            destDir = os.path.dirname(inputFileName)
#        print >> sys.stderr, "destDir:", destDir
        analizer.run(inputFileName, destDir)

    if sourceDir:
        if not destDir:
#            print >> sys.stderr, "Warning! Set destination dir will be ", sourceDir
            destDir = sourceDir
#        print >> sys.stderr, "destDir:", destDir
        names = getFilenameListFromDirectory(sourceDir)
        for name in names:
            analizer.run(name, destDir)

if __name__ == "__main__":
    main()
