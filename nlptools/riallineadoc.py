#!/usr/bin/python
#----------------------------------------------------------------------

#  Allinea i tag di divisione documento .


import sys
import codecs
from Utils import *

# use binary mode to avoid adding \r
if sys.platform == "win32":
	import os, msvcrt
	msvcrt.setmode(sys.stdout.fileno(), os.O_BINARY)

# if len(sys.argv) < 3:
# 	print 'usage:', sys.argv[0], 'file_con_DOC file_senza_DOC'
# 	sys.exit()

# main
def main(file_doc, file_parsed2, destination=None):
	sysFile1 = codecs.open(file_doc, encoding="utf-8", mode="r")
	destDir=None
	if len(sys.argv)>3:
		destDir=sys.argv[3]
        if destination != None:
          destDir = destination
	(file_parsed2, fileOutput, outputFileName) = openIOFiles(file_parsed2, destDir, ".parsed.doc")
	while True:
            line1 = sysFile1.readline()
            if line1== '':
                break
            if line1.startswith('<doc') or line1.startswith('</doc>'):
                print >> fileOutput, line1,
                continue
            else:
                line2 = file_parsed2.readline()
                print >> fileOutput, line2,

def riallinea(file_doc, file_parsed, destination=None):
  main(file_doc, file_parsed, destination)

#main(sys.argv[1],sys.argv[2])
