#!/usr/bin/env python

from maxent import MaxentModel
import sys
from itertools import *
import hashlib

def Fme_CreaModelloME(LabelsSamples, parametriME, modelFile):
    param=SettaParametriME(parametriME)
    TrainSet=Formatta(LabelsSamples[0], LabelsSamples[1])
    modello = CreaProblema(TrainSet, param)
    StampaModello(modello, modelFile, param)

"""
 Labels Samples: Primo elemento: lista delle labels (Gli outcomes del training)
 i valori della mappa sono i valori associati alla feature.
 sono delle mappe. Le chiavi delle mappa sono le features attive, mentre
 Secondo elemento, lista associata alla lista di labels in cui gli elementi.

 
"""
def Fme_CreaModelloMESparso(LabelsSamples, parametriME, modelFile,
                            SecondOrderFeatures=False):
    param=SettaParametriME(parametriME)
    TrainSet=FormattaSparso(LabelsSamples[0], LabelsSamples[1], SecondOrderFeatures)
    modello = CreaProblema(TrainSet, param)
    StampaModello(modello, modelFile, param)

def Fme_ApplicaME(ListaFeatures, modello, MostraProbabilita):
    contesto=FormattaTest(ListaFeatures)
 #    print modello, contesto
    if (MostraProbabilita):
        ProbabilityPrediction=modello.eval_all(contesto)
    else:
        ProbabilityPrediction=modello.eval(contesto)
    return ProbabilityPrediction

def Fme_ApplicaMESparso(ListaFeatures, modello, MostraProbabilita, SecondOrderFeatures=False):
    contesto=FormattaTestSparso(ListaFeatures, SecondOrderFeatures)
#    print modello, contesto
    if (MostraProbabilita):
        ProbabilityPrediction=modello.eval_all(contesto)
    else:
        ProbabilityPrediction=modello.eval(contesto)
    return ProbabilityPrediction

def Fme_CaricaModelloME(modelFile):
    model=MaxentModel()
    file=modelFile+'.me'
    model.load(file)
    return model

def FormattaTestSparso(ListaFeatures, SecondOrderFeature):
    Lista=[]
    res = CalcolaInsiemeValoriFeatures(ListaFeatures, SecondOrderFeature).encode('utf-8')
    Lista =  res.split()
    return Lista

def FormattaTest(ListaFeatures):
  Lista=[]
  for feat in ListaFeatures:
    featUTF=feat.encode('utf-8')
    Lista.append(featUTF)
  return Lista

def CreaProblema(TrainSet, param):
    modello=MaxentModel()
    modello.begin_add_event()
    for event in TrainSet:
        eventUTF=event.encode('utf-8')
        eventS=eventUTF.split()
        modello.add_event(eventS[1:], str(eventS[0]), TrainSet[event])
    modello.end_add_event()
    #print param
    modello.train(param['iteration'], param['algorithm'], param['GaussianPriorSmoothing'], param['TerminateTollerance'])
    #modello.train(20,'gis',0, 1E-05)
    return modello
        
def StampaModello(modello, modelFile, param):
    file=modelFile+'.me'
    if (param['compressed']):
        modello.save(file, True)
    else:
        
        modello.save(file)

def CalcolaValoreFeature(Samples_event, feat):
  feature_value = Samples_event[feat]
  if type(feature_value) in (float, int):
    return ''+feat+"_"+ ('%.3f' % (feature_value,)).rstrip('0').rstrip('.')
  elif type(feature_value) in  (str, unicode):
    return ''+feat+"_"+ feature_value
  else:
    print type(feature_value)
    assert(False)

def CalcolaInsiemeValoriFeatures(Samples_event, SecondOrderFeatures):
  sorted_keys = sorted(Samples_event.keys())
  not_to_combine_keys = [key for key in sorted_keys if key[0] in ['U' ,'B']]
  to_combine_keys = [key for key in sorted_keys if key[0] not in ['U' ,'B']]
  if SecondOrderFeatures:
    all_combinations = [combination for combination in combinations(to_combine_keys, 2)]
    combinated_strings = map(lambda x: CalcolaValoreFeature(Samples_event, x[0]) + '+' + CalcolaValoreFeature(Samples_event, x[1]), all_combinations)
    concatenated_strings = ' '.join(combinated_strings)
    not_combinated_strings = map(lambda key: CalcolaValoreFeature(Samples_event, key), not_to_combine_keys)
    not_concatenated_strings = ' '.join(not_combinated_strings)
    return  concatenated_strings + ' ' + not_concatenated_strings
  else:
    feature_values = map(lambda x: CalcolaValoreFeature(Samples_event, x), sorted_keys)
    return ' '.join(feature_values)

def FormattaSparso(Labels, Samples, SecondOrderFeatures):
    Eventi={}
    for event in range(len(Labels)):
        chiave=Labels[event]
        chiave += ' ' + CalcolaInsiemeValoriFeatures(Samples[event], SecondOrderFeatures)
        if chiave in Eventi:
          Eventi[chiave]+=1
        else:
          Eventi[chiave]=1
    return Eventi

def Formatta(Labels, Samples):
    Eventi={}
    for event in range(len(Labels)):
        chiave=Labels[event]
        for feat in Samples[event]:
            chiave+=' '+feat
        if chiave in Eventi:
            Eventi[chiave]+=1
        else:
            Eventi[chiave]=1
    return Eventi
        
def SettaParametriME(parametriME):
    i=0
    #valori di default:
    param={}
    param['algorithm']='gis'
    param['iteration']=200
    param['verbose']=1
    param['compressed']=1
    param['GaussianPriorSmoothing']=0
    param['TerminateTollerance']=0
    while (i<(len(parametriME)-1)):
        if parametriME[i] == '-a':
            param['algorithm']= parametriME[i+1]
        elif parametriME[i] == '-iter':
            param['iteration'] = int(parametriME[i+1])
        elif parametriME[i] == '-verbose':
            param['verbose'] = int(parametriME[i+1])
        elif parametriME[i] == '-gVar':
            param['GaussianPriorSmoothing'] = float(parametriME[i+1])
        elif parametriME[i] == '-compressed':
            param['compressed'] = int(parametriME[i+1])
        elif parametriME[i] == '-tTollerance':
            param['TerminateTollerance'] = float(parametriME[i+1])
        else:
            print >> sys.stderr,  "sconosciuta opzione per ME: ", parametriME[i]
        i+=2
    return param
