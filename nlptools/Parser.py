#$Id: Parser.py,v 1.5 2012-06-13 13:27:53 felice Exp $
import codecs
from desr import *
from Utils import *
from pkg_resources import resource_filename

MODULE_NAME = "t2k.catenapy.Parser"

class ParserPY ():

    debug = False
    ver = "$RCSfile: Parser.py,v $ $Revision: 1.5 $"

    def __init__(self,  encode='utf-8'):
        self.encode = encode

    def setDebug(self, debug):
        self.debug = debug

    def Parsing(self, inputFileName, destDir=None):
        self.inputFileName = inputFileName
        (self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".parsed")
        ifs = ifstream(inputFileName)
        s = self.c.sentenceReader(ifs)
        for sentence in s:
            pp = self.parser.parse(sentence)
            testoAnalizzato = self.c.toString(pp)
            self.StampaSuFile(testoAnalizzato)
        #self.outputFileName = renameTmpFileName(self.outputFileName)
        return self.outputFileName

    def StampaSuFile(self, testo):
        print >> self.fileOutput, testo.decode("utf-8")

    def CaricaParser(self, modelParser):
        self.modelParser = modelParser
        self.parser = Parser.create(modelParser)
        self.c = Corpus.create("it", "CoNLL")
        if (self.debug):
            print >> sys.stderr, 'Caricamento Parser eseguito'

    def CaricaParserWithProcess(self, modelParser):
        self.modelParser = modelParser

    def ParsingWithProcess(self, inputFileName, destDir=None):
        from subprocess import call, PIPE, Popen
        (self.fileInput, self.fileOutput, self.outputFileName)  = openIOFiles(inputFileName, destDir, ".parsed")
        z = Popen(["./desr", "-m", self.modelParser, inputFileName], stdout=PIPE)
        for line in z.stdout:
            self.StampaSuFile(line.rstrip('\n'))
        #self.outputFileName = renameTmpFileName(self.outputFileName)
        return self.outputFileName

def main ():
    modelParser="Modelli/en/Parser/EN_parser.mlp"

    p=ParserPY()
    p.CaricaParser(modelParser)

################################
    InputFile="pro4644693"
    p.Parsing(InputFile+".pos")


if __name__ == "__main__":
  import argparse
  arg_parser = argparse.ArgumentParser()
  arg_parser.add_argument("-m", "--model_name", help="the name of the model to be used (required)")
  arg_parser.add_argument("-i", "--input_file", help="the .pos file to be parsed")
  args = arg_parser.parse_args()
  input_file = args.input_file
  model_name = args.model_name
  if None in [model_name, input_file]:
      raise Exception("Mandatory argument missing")
  p = ParserPY()
  p.CaricaParser(model_name)
  #p.Parsing(input_file)
  p.ParsingWithProcess(input_file)

 #main()
 
