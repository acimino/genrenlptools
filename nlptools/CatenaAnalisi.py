from SentenceSplitter import SentenceSplitter
from Tokenizer import Tokenizer
from PosTagger import PosTagger
#from Parser import ParserPY

#############################################################################
#SSplitter:
ritorniACapo = True
confSentenceSplitterFile="Conf/it/sentenceSplitter.conf"
modelSentenceSplitterFile="Modelli/it/SentenceSplitter/ME-IT-TrainingCorpus"
#Tokenizer:
N_useNormalizer = True
normalizerRuleFileName = "Modelli/it/Normalizer/rule_normalize.txt"
T_useLexicon= True
fileParoleAccentate="Lessici/it/Tokenizer/paroleaccentate.txt"
ruleFileTokenizer="Modelli/it/Tokenizer/lex-ita-rule.txt"
#POStagger:
confPosTaggerFile="Conf/it/postaggerME.conf"
fileLessico="Lessici/it/PosTagger/lexicon.tanl"
fileFormario="Lessici/it/PosTagger/fullexMorph.tanl"
modelPosTaggerFile = "Modelli/it/PosTagger/modelloPOS_conLessico_isst-paisa-devLeg-testLeg"
morphSplit = True
training = False
dump = False
lingua = "IT"
#Parser
#modelParser="Modelli/it/Parser/Parser.trainingTOT.MLP"
##############################################################################

#Carico le strutture dati:

################################

def main():
  import argparse
  input_parser = argparse.ArgumentParser()
  input_parser.add_argument("-i", "--input_file", help="the .pos file to be analyzed if in analyzer mode, or .ner file in training mode (required)")
  args = input_parser.parse_args()
  inputFile = args.input_file
  destDir=""

  #FASE DI CARICAMENTO:
  #Sentence Splitter
  SSplitter=SentenceSplitter()
  SSplitter.setRitorniACapo(ritorniACapo)
  SSplitter.CaricaSentenceSplitter(confSentenceSplitterFile, modelSentenceSplitterFile)
  #Tokenizer
  tokenizer = Tokenizer()
  tokenizer.setUseLexicon(T_useLexicon)
  tokenizer.setUseNormalizer(N_useNormalizer)
  tokenizer.setNormalizerRuleFileName(normalizerRuleFileName)
  tokenizer.CaricaTokenizer(ruleFileTokenizer, fileParoleAccentate)
  #POS tagger
  posTagger = PosTagger()
  posTagger.setFileNameLessico(fileLessico)
  posTagger.setFileNameFormario(fileFormario)
  posTagger.setMophSplit(morphSplit)
  posTagger.setTraining(training)
  posTagger.setLingua(lingua)
  posTagger.CaricaPosTagger(confPosTaggerFile, modelPosTaggerFile)
  #Parser
  #parser = ParserPY()
  #parser.CaricaParser(modelParser)

  #FASE DI ANALISI:
  nextFileName = SSplitter.SentenceSplitting(inputFile, destDir)
  nextFileName = tokenizer.Tokenize(nextFileName, destDir)
  nextFileName = posTagger.PosTagging(nextFileName, destDir)
  #parsedFileName = parser.Parsing(nextFileName, destDir)

################################


if __name__ == "__main__":
  main()
