#!/usr/bin/python
# -*- coding: utf-8 -*-
#$Id: Tokenizer.py,v 1.4 2012-12-11 10:38:13 felice Exp $

import os
import sys
import re
import getopt
import codecs
import traceback
import string
from Token import Token
from Utils import *
from Normalizer import Normalizer
from pkg_resources import resource_filename

class Tokenizer:
    """Tokenizer """

    ver = "$RCSfile: Tokenizer.py,v $ $Revision: 1.4 $"
    matches = [] #parte sx della RE
    debug = False
    apexLexicon = None
    tokenizedText = []
    useLexicon = False
    encode = 'utf-8'
    useNormalizer = False

    def __init__(self):
        self.matches = []

    def setDebug(self, debug):
        self.debug = debug
        #print self.debug

    def setEncode(self, encode):
        self.encode = encode

    def setUseLexicon(self, use):
        if(self.debug):
            print "setUseLexicon: ", use
        self.useLexicon = use

    def setUseNormalizer(self,useNormalizer):
        self.useNormalizer = useNormalizer

    def setNormalizer(self,normalizer):
        self.normalizer = normalizer
        
    def setNormalizerRuleFileName(self, normalizerRuleFileName):
        self.normalizerRuleFileName = normalizerRuleFileName
   

    def printTokenizedText(self):
        for tok in self.tokenizedText:
            print >> self.fileOutput, tok.word
        print >> self.fileOutput
        self.tokenizedText=[]

    def printConf(self):
        print "Tokenizer conf:"
        for property, value in vars(self).iteritems():
            print property, ": ", value

    def parseRuleFile(self, filename):
        """
        parsa il file di espressioni regolari da applicare sul testo
        @param filename: nome del file delle espressioni regolari
        @type  text: string
        @param encode: tipo di encoding da usare per parsare il file
        @type  text: string
        """

#        file = codecs.open(filename, encoding=self.encode, mode='r')
        if (self.debug):
            print >> sys.stderr, "parseRuleFile(",filename,")"
        file = openFile(filename, self.encode, 'r')
        #legge riga per riga: ogni riga è composta da 2 parti, sx e dx:
        #sx è il match della regexp
        #dx è la sostituzione della regexp
        for line in file:
            if (line[0] == '#'):
                #riga commentata, la salto
                continue
#            print "prima(",line,")"
            line = line.strip('\n') #tolgo il ritorno a capo
            line = line.strip(' ') #tolgo il ritorno a capo
            if ( len(line) > 0 ):
                try:
                    self.matches.append(re.compile(line,re.U|re.I))
                except re.error, e:
                    print >> sys.stderr, "Error compiling:",line," - ", e
                    sys.exit(-1)


    def matchRE(self,text):
        bestMo = None
        for m in self.matches:
            try:
                mo = m.match(text)
                if ( mo != None ):
                    if (self.debug):
                        print >> sys.stderr,"matchRE pattern:",m.pattern.encode("utf-8")
                        print >> sys.stderr,"matchRE matcha:",text[mo.start():mo.end()]
                    if (None != bestMo):
                        if( mo.start() <= bestMo.start() ):
                            if (mo.end() > bestMo.end() ):
                            #trovato un match + grande, lo memorizzo
                                if (self.debug):
                                    print >> sys.stderr, "matchRE trovato un match + grande",
                                    text[mo.start(): mo.end()], "matchRE rispetto a:",
                                    text[bestMo.start():bestMo.end()]
                                bestMo = mo
                    else:
                        bestMo = mo

            except re.error, e:
                print >> sys.stderr, "Error in matchRE:", e, ", i=",i
                sys.exit(-1)
                i = i+1
        return bestMo

    def applyRE(self, text):
        """
        Aplica le espressioni regolari sul testo
        @param text: testo sul quale vanno applicate le espressioni regolare
        @type  text: string
        """
        bestMo = None
        id = 0
        self.tokenizedText = []
        while (True):
            id += 1
            text = text.lstrip() #elimino gli spazi in testa
            if(self.debug):
                print >> sys.stderr,"applyRE: text:",text
            bestMo = self.matchRE(text)
            if (bestMo != None):
                #se si trova un match
                if(self.debug):
                    print >> sys.stderr, "applyRE: best match(",bestMo.start(),bestMo.end(),text[bestMo.start():bestMo.end()],")"
                if(self.debug):
                    print >> sys.stderr, "applyRE:", text[bestMo.start():bestMo.end()]
                token = Token(text[bestMo.start():bestMo.end()])
                self.tokenizedText.append(token) #metto il match nella lista dei Token
                text = text[bestMo.end():]#vado avanti nel testo saltando quello che matchava
                bestMo = None
            else:
                #se non ho nessun match
                if (len(text)>0):
                    if (self.debug):
                        print >> sys.stderr, "niente in: (" +text[0:text.find(' ')]+") salto al prox spazio"

                    token = Token(text[:text.find(' ')])
                    #token.printToken()
                    self.tokenizedText.append(token)
                    text = text[text.find(' '):]
                else:
                    break

    def convertApexEnd(self):
        id = 0
        for token in self.tokenizedText:
            id += 1
            if (len(token.word)>=2):
                if ("'" in token.word):
                    if(self.debug):
                        print >> sys.stderr,"termina con apice:",token
                    #controlla se compare nella hash degli apici
                    ltoken = token.word.lower()
                    if(ltoken in self.apexLexicon):
                        #converto vocale + apice in vocale accentata
                        if(self.debug):
                            print >> sys.stderr, token, "->", self.apexLexicon[ltoken]
                        token.setWord(token.word[:-2]+self.apexLexicon[ltoken][-1])
                        token.setId(id)
                    else:
                        if(self.debug):
                            print >> sys.stderr, token, "non è nella lista delle parole accentate", token[-2]
                        if(token.word[-2] in "aeiou"):
                            token.setWord(token.word[:-1])
                            token.setId(id)
                            id += 1
                            self.tokenizedText.insert(id - 1, Token('\'', id))

    def loadApexLexicon(self,lexiconFile):
        try:
            self.apexLexicon = {}
            hashAccent = {u"à":"a'",u"è":"e'",u"é":"e'",u"ì":"i'",u"ò":"o'",u"ù":"u'"}
            fileIn = codecs.open(lexiconFile, encoding=self.encode, mode='r')
            for line in fileIn:
                accented = line.strip()
#                if(self.debug):
#                    print >> sys.stderr, "accendeted:", accented, accented[-1], accented[:-1]
                apexend = accented[:-1] + hashAccent[accented[-1]]
                self.apexLexicon[apexend] = accented
            fileIn.close()
        except IOError, e:
            usage()
            print >> sys.stderr ,"Error reading lexiconFile",e
            sys.exit(-1)

    def CaricaTokenizer(self, ruleFile, fileParoleAccentate=None):

        #carica il lessico delle parole accentate
        if (self.useLexicon):
            self.loadApexLexicon(fileParoleAccentate)
        #carico le regole di match
        self.parseRuleFile(ruleFile)
        
        if (self.useNormalizer):
            self.loadNormalizer()
        
    def Tokenize(self, inputFileName, destDir=None): #, outputFile=''):

        #        self.openIOFile(inputFileName)
        self.inputFileName = inputFileName
        (self.fileInput, self.fileOutput, self.outputFileName) = openIOFiles(inputFileName, destDir, ".tok.tmp")

        #tokenizzo il file di input riga x riga
        for line in self.fileInput:
            try:
                if (len (line) != 0):
                    if (self.useNormalizer):
                        line = self.normalizer.normalizeText(line)
                    self.applyRE(line)
                    ## ogni token vedo se hanno apice alla fine
                    if (self.useLexicon): 
                        self.convertApexEnd()
                    ## X Evitare problemi di memoria si deve stampare cio' che 
                    ## applyRE produce (tokenizedText) e successivamente svuotarlo
                    #QUIII!!!
                    self.printTokenizedText()
            except Exception, e:
                print >> sys.stderr, "Error:", e,"-", line
                traceback.print_exc(file=sys.stderr)
                try:
                    print  "Error 0", e
                except UnicodeDecodeError:
                    print " Error 1\n", line
                except UnicodeEncodeError:
                    print " Error 2\n", line
        #self.fileInput.close()
        self.outputFileName = renameTmpFileName(self.outputFileName)

        return self.outputFileName

    def loadNormalizer(self):
        
        normalizer = Normalizer()
        normalizer.setEncode(self.encode)
        normalizer.setRuleFile(self.normalizerRuleFileName)
        normalizer.loadRules()
        self.setNormalizer(normalizer)


def printTokenizedText(tokenizedText, outputFile):
        
    try:
        fileOut = codecs.open(outputFile, encoding='utf-8', mode='w')
    except IOError, e:
        usage()
        print >> sys.stderr ,"Error in outputFile",e
        sys.exit(-1)

    for tok in tokenizedText:
        print >> fileOut, tok.word
    fileOut.close()

def usage():

    print """
    Usage:
    python Tokenizer.py [options] 

    Options:
    -i inputFile          nome del file di input
    -o outputFile         nome del file di output
    -r ruleFile           nome del file di regole [italian-rule.txt]
    -e encoding           encoding dei file (sia di regole che di input/output) [utf-8] (utf-8/latin-1)
    -a                    converte le vocale-accento di fine parola in lettere accentate (se nel formario)
    -n normalizeRuleFile  nome del file delle regole per la normalzizzazione
    -d                    attiva il debug
    """

def main():
    MODULE_NAME = "t2k.catenapy.Tokenizer"
    inputFile = ""
    outputFile = ""
    try:
        opts, args = getopt.getopt(sys.argv[1:], "n:hi:o:r:e:dva", ["normalize=","accent", "debug", "input=", "help", "output=","rule=", "encoding="])
    except getopt.GetoptError, err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    #valori di default
    output = None
    ruleFile = resource_filename(MODULE_NAME, "Modelli/it/Tokenizer/lex-ita-rule.txt")
    encode = 'utf-8'
    debug = False
    useLexicon = False
    useNormalizer = False
    normalizerRuleFileName = resource_filename(MODULE_NAME, "Modelli/it/Normalizer/rule_normalize.txt")
    #parsing degli argomenti
    for o, a in opts:
        if o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-o", "--output"):
            outputFile = a
        elif o in ("-i", "--input"):
            inputFile = a
        elif o in ("-r", "--rule"):
            ruleFile = a
        elif o in ("-e", "--encoding"):
            encode = a
        elif o in ("-d", "--debug"):
            debug = True
        elif o in ("-n", "--normalize"):
             normalizerRuleFileName = a
             useNormalizer = True
        elif o in ("-a", "--accent"):
            useLexicon = True
        else:
            assert False, "unhandled option"
    #UTF-8 support

    scriptpath = os.path.abspath(os.path.dirname(sys.argv[0]))

    tokenizer = Tokenizer()
    tokenizer.setUseLexicon(useLexicon)
    tokenizer.setDebug(debug)
    tokenizer.setEncode(encode)
    tokenizer.setUseNormalizer(useNormalizer)
    tokenizer.setNormalizerRuleFileName(normalizerRuleFileName)
#    tokenizer.setOutputFile(outputFile)
    #if (useNormalizer):
    #tokenizer.loadNormalizer(normalizerRuleFileName)
    tokenizer.CaricaTokenizer(ruleFile, resource_filename(MODULE_NAME, "Lessici/it/Tokenizer/paroleaccentate.txt"))
    tokenizer.Tokenize(inputFile)
    #printTokenizedText(tokenizer.tokenizedText, outputFile)

if __name__ == "__main__":
    main()

