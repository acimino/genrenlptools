#/usr/bin/env python

from svmutil import *
from svm import *
import sys

def Fsvm_CreaModelloSVM(LabelsSamples, parametriSVM, modelFile):
    LabelsSamplesIndicizzate=Indicizza(LabelsSamples, modelFile)
    problem = CreaProblema(LabelsSamplesIndicizzate[0], LabelsSamplesIndicizzate[1])
    param=SettaParametriSVM(parametriSVM)
    model = CreaModello(problem, param)
    svm_save_model(modelFile+'.svm', model) #nuovo


def Fsvm_Denso_CreaModelloSVM(LabelsSamples, parametriSVM, modelFile):
    problem  = CreaProblema(LabelsSamples[0], LabelsSamples[1])
    param=SettaParametriSVM(parametriSVM)
    model = Denso_CreaModello(problem,param)
    svm_save_model(modelFile+'.svm', model)


## def Fsvm_Denso_CreaModelloSVM(LabelsSamples, parametriSVM, modelFile):
##     problem  = CreaProblema(LabelsSamples[0], LabelsSamples[1])
##     param=SettaParametriSVM(parametriSVM)
##     model = CreaModello(problem,param)
##     svm_save_model(modelFile+'.svm', model)
    #model.save(modelFile+'.svm')

def Fsvm_Denso_ApplicaSVM(ListaFeatures, modello):
    labels, acc, vals = Denso_PrediciProbabilita(ListaFeatures, modello)
    risultato=(labels, acc, vals)
    return risultato

#interfacciato direttamente al C: restituisce solo la label vincente
def Fsvm_ApplicaSVM(ListaFeatures, modello, modelloIndici):
    ListaFeaturesIndicizzata=Indicizza_feature(ListaFeatures, modelloIndici)
    #labels, acc, vals = Denso_PrediciProbabilita(ListaFeaturesIndicizzata, modello)
    labels = PrediciProbabilita(ListaFeaturesIndicizzata, modello)
    labels = TraduciLabels_n(labels, modelloIndici)
    risultato=labels
    return risultato

#interfacciato attraverso gli script python, restituisce informazioni
#sulla probabilita di assegnamento per ogni classe
def Fsvm_ApplicaSVM_2(ListaFeatures, modello, modelloIndici):
    ListaFeaturesIndicizzata=Indicizza_feature(ListaFeatures, modelloIndici)
    labels, acc, vals = Denso_PrediciProbabilita(ListaFeaturesIndicizzata, modello)
    labels = TraduciLabels2(labels, modelloIndici)
    risultato=(labels, acc, vals)
    return risultato

def Fsvm_CaricaIndici(modelFile):
    fileModelloIndici=open(modelFile, 'r')
    labels=False
    feat=False
    Labels={}
    Feat={}
    while True:
        line=fileModelloIndici.readline()
        line=line.decode('utf-8')
        lineS=(line.strip()).split('\t')
        if line=='':
            break
        elif (lineS[0]=='F-FEATS'):
            labels=False
            feat=True
            continue
        elif (lineS[0]=='F-LABELS'):
            labels=True
            feat=False
            continue
        else:
            if (feat):
                Feat[lineS[0]]=int(lineS[1])
            elif (labels):
                Labels[float(lineS[1])]=lineS[0]
            else:
                print >> sys.stderr, 'Errore caricamento file degli indici per SVM'
                Labels={}
                Feat={}
                exit()
    return (Labels, Feat)

def TraduciLabels_n(label, modelloIndici):
    Tlabels=[]
    Indici_Label=modelloIndici[0]
    #Tlabels.append(Indici_Label[label])
    if label in Indici_Label:
        Tlabels.append(Indici_Label[label])
    else:
        Tlabels.append("PROBLEMA")
    return Tlabels

def TraduciLabels2(labels, modelloIndici):
    Tlabels=[]
    Indici_Label=modelloIndici[0]
    for label in labels:
        if label in Indici_Label:
            Tlabels.append(Indici_Label[label])
        else:
            Tlabels.append(str(Indici_Label[label]))
    return Tlabels

def Fsvm_CaricaModelloSVM(modelFile):
    model = svm_load_model(modelFile+'.svm')  
    #modelloIndici = Fsvm_CaricaIndici(modelFile)
    return model#, modelloIndici

def Fsvm_Denso_CaricaModelloSVM(modelFile):
    model = svm_load_model(modelFile+'.svm')  
    return model

def Predici(samples, modello):
    prediction = modello.predict(samples)
    return prediction

def Denso_PrediciProbabilita(ListaFeatures, modello):
    return svm_predict([0]*1, [ListaFeatures], modello, '-b 1')

def PrediciProbabilita(ListaFeatures, modello):
    x0, max_idx = gen_svm_nodearray(ListaFeatures)
    label = libsvm.svm_predict(modello, x0)
    return label

def PrediciProbabilita_1(ListaFeatures, modello):
    #x0, max_idx = gen_svm_node_array(ListaFeatures)
    #label = libsvm.svm_predict(modello, x0)
    return svm_predict([0]*1, [ListaFeatures], modello, '-b 1')

def Indicizza_feature(ListaFeatures, modelloIndici):
    ListaFeaturesIndicizzata={}
    Indici_Feat=modelloIndici[1]
    for feat in ListaFeatures:
        if feat in Indici_Feat:
            ListaFeaturesIndicizzata[Indici_Feat[feat]]=1
    return ListaFeaturesIndicizzata

def CreaModello(problem,param):
    model_ptr = libsvm.svm_train(problem,param)
    model = toPyModel(model_ptr)
    return model

def Denso_CreaModello(problem,param):
    model_ptr = libsvm.svm_train(problem,param)
    model = toPyModel(model_ptr)
    #model = svm_train(problem,param)
    return model

def CreaProblema(labels, samples):
    problem = svm_problem(labels, samples)
    return problem

def SettaParametriSVM(parametriSVM):
    i=0
    #valori di default:
    param=svm_parameter()
    param.svm_type = 0;
    param.kernel_type = 2;
    param.degree = 3;
    param.gamma = 0;   # 1/k
    param.coef0 = 0;
    param.nu = 0.5;
    param.cache_size = 1000;
    param.C = 100;
    param.eps = 1e-3;
    param.p = 0.1;
    param.shrinking = 1;
    param.probability = 0;
    param.nr_weight = 0;
    #param.weight_label = NULL;
    #param.weight = NULL;
    while (i<(len(parametriSVM)-1)):
        if parametriSVM[i] == '-s':
            param.svm_type = int(parametriSVM[i+1])
        elif parametriSVM[i] == '-t':
            param.kernel_type = int(parametriSVM[i+1])
        elif parametriSVM[i] == '-d':
            param.degree = int(parametriSVM[i+1])
        elif parametriSVM[i] == '-g':
            param.gamma = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-r':
            param.coef0 = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-n':
            param.nu = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-m':
            param.cache_size = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-c':
            param.C = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-e':
            param.eps = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-p':
            param.p = float(parametriSVM[i+1])
        elif parametriSVM[i] == '-h':
            param.shrinking = int(parametriSVM[i+1])
        elif parametriSVM[i] == '-b':
            param.probability = int(parametriSVM[i+1])
        else:
            print >> sys.stderr,  "sconosciuta opzione SVM: ", parametriSVM[i]
        i+=2
        #elif parametriSVM[i] == '-w': l'opzione w serva per dati sbilanciati...settare in futuro
    return param
    
def Indicizza(LabelsSamples, modelFile):
    label=0
    feat=0
    LabelsID={}
    FeatID={}
    IDLabels=[]
    IDevent={}
    IDSamples=[]
    FileModel = open(modelFile, 'w')
    Labels=LabelsSamples[0]
    Samples=LabelsSamples[1]
    for i in range(len(Labels)):
        if Labels[i] in LabelsID:
            IDLabels.append(LabelsID[Labels[i]])
        else:
            label+=1
            LabelsID[Labels[i]]=label
            IDLabels.append(label)
        for event in Samples[i]:
            if event in FeatID:
                IDevent[FeatID[event]]=1    
            else:
                feat+=1
                FeatID[event]=feat
                IDevent[feat]=1
        IDSamples.append(IDevent)
        IDevent={}
    #Stampo il file con il mapping indicizzato
    #Prima le label:
    print >> FileModel, 'F-LABELS'
    for lab in LabelsID:
        print >> FileModel, '\t'.join([lab.encode('utf-8'), str(LabelsID[lab])])
    #Prima le features:
    print >> FileModel, 'F-FEATS'
    for fea in FeatID:
        print >> FileModel, '\t'.join([fea.encode('utf-8'), str(FeatID[fea])])
    return (IDLabels, IDSamples)
