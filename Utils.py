import codecs
import sys
import re
import os
import logging

def openFile(fileName, enc='utf-8', _mode='r'):
    file = None
    try:
        #print >> sys.stderr , fileName, enc, _mode
        file = codecs.open(fileName, encoding=enc, mode=_mode)
    except IOError, e:
        print >> sys.stderr , "Error in Opening file (", fileName,") =>", e
        sys.exit(-1)
    return file

def removeFileExt2 (text):
    return os.path.basename(text)

def removeFileExt (text):
    
    try:
#        pattern = re.compile('([\s\w]+[\/\w]*)\.?.*')
        pattern = re.compile('(.+)\..*')
        basename = pattern.match(text).group(1)
    except IndexError, e:
           print >> sys.stderr , "IndexError in removeFileExt (", text,") =>", e
           sys.exit(-1)
    except AttributeError, e:
#           print >> sys.stderr , "AttributeError in removeFileExt (", text,") =>", e
           #sys.exit(-1)
           basename = text
#    print >> sys.stderr , "basename:", basename
    return basename


def str2bool(v):
    return v.lower() in ("yes", "true", "t", "1")


def getLogLevel(s):

    levels = {"debug" : logging.DEBUG, "info": logging.INFO, "warning": logging.WARNING, 
              "error": logging.ERROR, "critical": logging.CRITICAL}
    l = logging.WARNING

    if s:
        if s.lower() in levels:
            l = levels[s.lower()]
    
    return l

def getFileExtensionFromTask (task):
    exts = {"txt": ".txt", 
            "ss" : ".split", 
            "tk" : ".tok", 
            "pt" : ".pos", 
            "pa" : ".parsed", 
            "rdb" : ".rdb", "rdl" : ".rdl", "rds" : ".rds", "rdg" : ".rdg",
            "rfb" : ".rfb", "rfl" : ".rfl", "rfs" : ".rfs", "rfg" : ".rfg",
            "te" : ".term", 
            "mo" : ".monit",
            "pr" : ".prz"}
    
    if ( task.lower() in exts ):
        fext = exts[task.lower()]
    else:
        print >> sys.stderr, "extension for task %s doen't exists!" % task
        fext = None

    return fext


def getReadType(s):

    type = {"rfb" : 0, "rfl" : 0, "rfs" : 0, "rfg" : 0, "rdb" : 0, "rdl" : 0, "rds" : 0, "rdg" : 0}
    if s:
        keys = s.split(',')
        for value in keys:
            if value.lower() in ('rf', 'rfb'):
                type["rfb"] = 1;
            if value.lower() in ('rf', 'rfl'):
                type["rfl"] = 1;
            if value.lower() in ('rf', 'rfs'):
                type["rfs"] = 1;
            if value.lower() in ('rf', 'rfg'):
                type["rfg"] = 1;
            if value.lower() in ('rd', 'rdb'):
                type["rdb"] = 1;
            if value.lower() in ('rd', 'rdl'):
                type["rdl"] = 1;
            if value.lower() in ('rd', 'rds'):
                type["rds"] = 1;
            if value.lower() in ('rd', 'rdg'):
                type["rdg"] = 1;

    return type


def getLevels(s):

    levels = {"txt" : 1, "ss" : 0, "tk" : 0, "pt" : 0, "pa" : 0, "mo" : 0, "te" : 0, "re" : 0, "pr" : 0,
              "rdg" : 0, "rdb" : 0, "rdl" : 0, "rds" : 0, "rfb" : 0, "rfl" : 0, "rfs" : 0, "rfg" : 0}
    
    set_of_levels = ('ss','tk','pt','pa','rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg','te','mo')
    if s:
        keys = s.split(',')
        for value in keys:
            v = value.strip()
            if v.lower() in ('ss','tk','pt','pa','rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg','te','mo','pr'):
                levels["ss"] = 1;
            else:
                print >> sys.stderr, "Warning: No %s level key found!" % v
            if v.lower() in ('tk','pt','pa','rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg','te','mo', 'pr'):
                levels["tk"] = 1;
            if v.lower() in ('pt','pa','rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg','te','mo', 'pr'):
                levels["pt"] = 1;
            if v.lower() in ('pa','rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg','te','mo', 'pr'):
                levels["pa"] = 1;
            if v.lower() in ('rdg','rdb','rdl','rds','rfb','rfl','rfs','rfg'):
                levels[v.lower()] = 1;
            if v.lower() in ('te'):
                levels["te"] = 1;
            if v.lower() in ('mo'):
                levels["mo"] = 1;
            if v.lower() in ('pr'):
                levels["pr"] = 1;

    return levels


def getFilenameListFromDirectory1 (path):

    try:
        fileNameList = os.listdir(path)
    except OSError as e:
        print >> sys.stderr, e
        sys.exit(-1)
    else:
        return fileNameList


def getFilenameListFromDirectory (top, ext=None):

    nameList = []
    for root, dirs, files in os.walk(top):
        for name in files: 
#            print >> sys.stderr,"ext:",os.path.splitext(name)[1]
            nameExt = os.path.splitext(name)[1]
            if not ext or ( ext and (nameExt == ext or nameExt == "."+ext)):
                    print >> sys.stderr,os.path.join(root, name)
                    nameList.append(os.path.join(root, name))

    return nameList


def renameTmpFileName (filename):
    newFilename = removeFileExt(filename)
    #print "rename from %s to %s" % (filename, newFilename)
    os.rename(filename,newFilename)
    return newFilename

def openIOFiles(inputFileName, destDir, ext, encode="utf-8"):
    #print "openIOFiles:",inputFileName, destDir, ext, encode
    fileInput = openFile(inputFileName, encode, 'r')
    fileOutput = "" 
    outputFileName = removeFileExt(os.path.basename(inputFileName))

    if  destDir: #se ho specificato la directory di destinazione
        outputFileName = os.path.join(destDir,removeFileExt(os.path.basename(inputFileName)))
    if ext:
        outputFileName = outputFileName + ext
        #    print "openIOFiles (%s)" % outputFileName
        fileOutput = openFile(outputFileName, encode, 'w')
        
    return (fileInput, fileOutput, outputFileName)
    
def openIOFilesNoOutput(inputFileName, mode="r", encode="utf-8"):
    fileInput = openFile(inputFileName, encode, mode)
    return fileInput



    
def printHash (hash, limit=-1):
    
    l = 0
    for k, v in hash.iteritems():
        l = l + 1
        if limit > -1:
            if l < limit:
                print k, v
            else:
                break
        else:
            print k, v

def saveToFile(filename, content):
    #print >> sys.stderr, "filename %s, content %s" %(filename,content)
    f = openFile(filename, _mode= "w")
    print >> f, content
    f.close()



def check_pid_filename(pid_filename):        
    """ Check For the existence of a unix pid. """

    f = open(pid_filename)
    pid = f.read()
    f.close()
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True


def check_pid(pid):        
    """ Check For the existence of a unix pid. """
    try:
        os.kill(pid, 0)
    except OSError:
        return False
    else:
        return True

def main():
    print >> sys.stderr, removeFileExt("a/pippo.txt")
    print >> sys.stderr, removeFileExt("a/p i p po.txt")
    print >> sys.stderr, removeFileExt("a/p' i p po.txt")
    print >> sys.stderr, removeFileExt("a/p' i p po.")
    print >> sys.stderr, removeFileExt("a/p' i p.po.po")
    print >> sys.stderr, removeFileExt("/tmp/filename.split.tmp")
    if False:
        names = getFilenameListFromDirectory (".","py")
        for name in names:
            print >> sys.stderr, name


if __name__ == "__main__":
    main()

    
