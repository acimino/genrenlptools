#!/bin/bash
args=("$@")
choosen_configuration=${args[0]}

training_and_test_folder="training_and_test"
files=( "EducationalTest.txt"  "Educational.txt"  "JournalismTest.txt"  "Journalism.txt"  "LiteratureTest.txt" "Literature.txt"  "ScientificProseTest.txt"  "ScientificProse.txt")

 for i in "${files[@]}"
 do
      echo "Executing postagging and syntactic parsing of $i"
      cd nlptools
      filename="${i%.*}"
      python Analizer.py -i ../training_and_test/$i > /dev/null 2>&1
      cp ../training_and_test/$filename.parsed.doc ../training_and_test/$filename.parsed
      cd ..
 done

parsed_files=("$training_and_test_folder/Educational.parsed"  "$training_and_test_folder/Journalism.parsed" \
    "$training_and_test_folder/Literature.parsed" "$training_and_test_folder/ScientificProse.parsed")

test_parsed_files=("$training_and_test_folder/JournalismTest.parsed" "$training_and_test_folder/EducationalTest.parsed" "$training_and_test_folder/LiteratureTest.parsed" "$training_and_test_folder/ScientificProseTest.parsed")


for i in "${test_parsed_files[@]}"
do
    cp $i.doc $i
done

function generate_conf_features {
    conf=$1
    echo $conf
    echo "Processing $conf file.."
    ALL_PARSED_FILES=$(IFS=" "; echo "${parsed_files[*]}")
    for i in "${parsed_files[@]}"
    do
       echo ""
       echo "python genre_feature_generator.py -c $conf -t -i $i -a \"$ALL_PARSED_FILES\""
       python genre_feature_generator.py -c $conf -t -i $i -a "$ALL_PARSED_FILES"
    done
    ALL_PARSED_TEST_FILES=$(IFS=" "; echo "${test_parsed_files[*]}")
    python genre_feature_generator.py -c $conf -t  -z  -a "$ALL_PARSED_TEST_FILES"

}

if [ -z $choosen_configuration ]; then
    for conf in Confs/*.conf
    do
        generate_conf_features $conf
    done
else
    generate_conf_features $choosen_configuration
fi





